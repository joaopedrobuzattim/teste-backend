const { ensureAuthenticated } = require('./ensureAuthenticated');
const { restrictTo } = require('./restrictTo');
const { globalErrorHandler } = require('./globalErrorHandler');

module.exports = {
  ensureAuthenticated,
  restrictTo,
  globalErrorHandler,
};

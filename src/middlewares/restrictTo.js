const { StatusCodes } = require('http-status-codes');
const { AppError } = require('../utils/index');
const userRepository = require('../repositories/user.repository');

module.exports.restrictTo = (role) => async (request, _response, next) => {
  const { id } = request.user;

  try {
    const user = await userRepository.getById(id);

    if (!user || !user.is_active) { throw new AppError('User not found!', StatusCodes.NOT_FOUND); }

    if (user.role !== role) { throw new AppError('You are not allowed to access this route!', StatusCodes.UNAUTHORIZED); }

    return next();
  } catch (error) {
    throw new AppError('You are not allowed to access this route!', StatusCodes.UNAUTHORIZED);
  }
};

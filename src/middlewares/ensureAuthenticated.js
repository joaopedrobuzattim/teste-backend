const { verify } = require('jsonwebtoken');
const { StatusCodes } = require('http-status-codes');
const { AppError } = require('../utils/index');
const authConfig = require('../config/auth/auth.config');

module.exports.ensureAuthenticated = (request, _response, next) => {
  const authHeader = request.headers.authorization;

  if (!authHeader) {
    throw new AppError('JWT token is missing!', StatusCodes.UNAUTHORIZED);
  }

  const [, token] = authHeader.split(' ');

  try {
    const decoded = verify(token, authConfig.jwt.secret);
    const { sub } = decoded;

    request.user = {
      id: sub,
    };

    return next();
  } catch (error) {
    throw new AppError('Invalid JWT token', StatusCodes.UNAUTHORIZED);
  }
};

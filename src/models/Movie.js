module.exports = (sequelize, DataTypes) => {
  const Movie = sequelize.define('Movie', {
    id: {
      type: DataTypes.UUIDV4,
      primaryKey: true,
    },
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    director_id: DataTypes.UUID,
    duration: DataTypes.INTEGER,
  });

  Movie.associate = (models) => {
    Movie.belongsTo(models.Director, { foreignKey: 'director_id' });
    Movie.belongsToMany(models.Genre, { through: 'movies-genres', foreignKey: 'movie_id' });
    Movie.hasMany(models.Vote, { foreignKey: 'movie_id' });
  };

  /* I Was getting a problem with the average calculation with Sequelize.
  So, I decided to crate a Hook to do it */
  Movie.prototype.toJSON = function () {
    const movie = { ...this.get() };
    if ('Votes' in movie) {
      const amountOfVotes = movie.Votes.length;
      const reduce = ((accumulator, currentValue) => accumulator + currentValue.dataValues.rating);
      const ratingsSum = movie.Votes.reduce(reduce, 0);
      const ratingAverage = (ratingsSum / amountOfVotes).toFixed(2);
      movie.Votes = {
        ratingAverage: amountOfVotes === 0 ? 0 : ratingAverage,
        amountOfVotes,
      };
      return movie;
    }
    return movie;
  };

  return Movie;
};

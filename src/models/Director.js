module.exports = (sequelize, DataTypes) => {
  const Director = sequelize.define('Director', {
    id: {
      type: DataTypes.UUIDV4,
      primaryKey: true,
    },
    name: DataTypes.STRING,
  });

  Director.associate = (models) => {
    Director.hasMany(models.Movie, { foreignKey: 'director_id' });
  };

  return Director;
};

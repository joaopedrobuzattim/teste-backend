module.exports = (sequelize, DataTypes) => {
  const Genre = sequelize.define('Genre', {
    id: {
      type: DataTypes.UUIDV4,
      primaryKey: true,
    },
    name: DataTypes.STRING,
  });

  Genre.associate = (models) => {
    Genre.belongsToMany(models.Movie, { through: 'movies-genres', foreignKey: 'genre_id' });
  };

  return Genre;
};

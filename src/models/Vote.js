module.exports = (sequelize, DataTypes) => {
  const Vote = sequelize.define('Vote', {
    id: {
      type: DataTypes.UUIDV4,
      primaryKey: true,
    },
    movie_id: {
      type: DataTypes.UUID,
    },
    user_id: {
      type: DataTypes.UUID,
    },
    rating: {
      type: DataTypes.INTEGER,
      validate: {
        min: 0,
        max: 4,
      },
    },
  });

  Vote.associate = (models) => {
    Vote.belongsTo(models.Movie, { foreignKey: 'movie_id' });
  };

  return Vote;
};

const yup = require('yup');
const { AppError } = require('../utils');

module.exports = {

  async createDirectorValidate(request, _response, next) {
    try {
      const schema = yup.object().shape({
        name: yup.string().required('You must provide a name!'),
      });

      await schema.validate(request.body, { stripUnknown: true });

      next();
    } catch (error) {
      throw new AppError(error.message);
    }
  },
  async updateDirectorValidate(request, _response, next) {
    try {
      const schema = yup.object().shape({
        name: yup.string(),
      });

      await schema.validate(request.body, { stripUnknown: true });

      next();
    } catch (error) {
      throw new AppError(error.message);
    }
  },

};

const authValidate = require('./auth.validate');
const userValidate = require('./user.validate');
const movieValidate = require('./movie.validate');
const genreValidate = require('./genre.validate');
const directorValidate = require('./director.validate');
const voteValidate = require('./vote.validate');

module.exports = {
  userValidate,
  authValidate,
  movieValidate,
  genreValidate,
  directorValidate,
  voteValidate,
};

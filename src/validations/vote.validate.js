const yup = require('yup');
const { AppError } = require('../utils');

module.exports = {

  async createVoteValidate(request, _response, next) {
    try {
      const schema = yup.object().shape({
        rating: yup.number()
          .required('You must provide a rating!')
          .min(0, 'Only values between 0 and 4 are allowed!')
          .max(4, 'Only values between 0 and 4 are allowed!'),
      });

      await schema.validate(request.body, { stripUnknown: true });

      next();
    } catch (error) {
      throw new AppError(error.message);
    }
  },
  async updateVoteValidate(request, _response, next) {
    try {
      const schema = yup.object().shape({
        rating: yup.number()
          .min(0, 'Only values between 0 and 4 are allowed!')
          .max(4, 'Only values between 0 and 4 are allowed!'),
      });

      await schema.validate(request.body, { stripUnknown: true });

      next();
    } catch (error) {
      throw new AppError(error.message);
    }
  },

};

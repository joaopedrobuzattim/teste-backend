const yup = require('yup');
const { AppError } = require('../utils');

module.exports = {

  async signupValidate(request, _response, next) {
    try {
      const schema = yup.object().shape({
        name: yup.string().required('You must provide a name!').min(4, 'Name is too short - should be 8 chars minimum'),
        email: yup.string().email('You must provide a valid email address!').required('You must provide a valid email email!'),
        password: yup.string().required('You must provide a password!').min(8, 'Password is too short - should be 8 chars minimum.'),
      });

      await schema.validate(request.body, { stripUnknown: true });

      next();
    } catch (error) {
      throw new AppError(error.message);
    }
  },
  async loginValidate(request, _response, next) {
    try {
      const schema = yup.object().shape({
        email: yup.string().email('You must provide a valid email address!').required('You must provide a valid email email!'),
        password: yup.string().required('You must provide a password!'),
      });

      await schema.validate(request.body, { stripUnknown: true });

      next();
    } catch (error) {
      throw new AppError(error.message);
    }
  },

};

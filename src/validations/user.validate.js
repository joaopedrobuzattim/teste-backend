const yup = require('yup');
const { AppError } = require('../utils');

module.exports = {

  async updateCurrentValidate(request, _response, next) {
    try {
      const schema = yup.object().shape({
        name: yup.string().min(4, 'Name is too short - should be 8 chars minimum'),
        email: yup.string().email('You must provide a valid email address!'),
        password: yup.string().min(8, 'Password is too short - should be 8 chars minimum.'),
      });

      await schema.validate(request.body, { stripUnknown: true });

      next();
    } catch (error) {
      throw new AppError(error.message);
    }
  },

  async createUserValidate(request, _response, next) {
    try {
      const schema = yup.object().shape({
        name: yup.string().min(4, 'Name is too short - should be 8 chars minimum').required('You must provie a name!'),
        email: yup.string().email('You must provide a valid email address!').required('You must provide an email!'),
        password: yup.string().required('You must provide a password!').min(8, 'Password is too short - should be 8 chars minimum.'),
        role: yup.string().required('You must provide a role!').oneOf(['user', 'admin'], 'Role must be user or admin!'),
      });

      await schema.validate(request.body, { stripUnknown: true });

      next();
    } catch (error) {
      throw new AppError(error.message);
    }
  },

  async updateUserValidate(request, _response, next) {
    try {
      const schema = yup.object().shape({
        name: yup.string().min(4, 'Name is too short - should be 8 chars minimum'),
        email: yup.string().email('You must provide a valid email address!'),
        password: yup.string().min(8, 'Password is too short - should be 8 chars minimum.'),
        role: yup.string().oneOf(['user', 'admin'], 'Role must be user or admin!'),
      });

      await schema.validate(request.body, { stripUnknown: true });

      next();
    } catch (error) {
      throw new AppError(error.message);
    }
  },

};

const yup = require('yup');
const { AppError } = require('../utils');

module.exports = {

  async createMovieValidate(request, _response, next) {
    try {
      const schema = yup.object().shape({
        name: yup.string().required('You must provide a name!'),
        description: yup.string().required('You must provide a description!'),
        duration: yup.number().required('You must provide a duration!').min(1, 'Invalid duration!'),
        director_id: yup.string().required('You must provide a director!'),
        genres_id: yup.array().of(yup.string()).required('You must provide a genre!'),
      });

      await schema.validate(request.body, { stripUnknown: true });

      next();
    } catch (error) {
      throw new AppError(error.message);
    }
  },
  async updateMovieValidate(request, _response, next) {
    try {
      const schema = yup.object().shape({
        name: yup.string(),
        description: yup.string(),
        duration: yup.number().min(1, 'Invalid duration!'),
        director_id: yup.string(),
        genres_id: yup.array().of(yup.string()),
      });

      await schema.validate(request.body, { stripUnknown: true });

      next();
    } catch (error) {
      throw new AppError(error.message);
    }
  },

};

const { Movie } = require('../models');

module.exports = {
  list: (query) => Movie.findAll(query),
  create: (params) => Movie.create(params),
  getById: (id) => Movie.findByPk(id),
  getOne: (query) => Movie.findAll(query),
  update: (movie) => movie.save(),
  destroy: (id) => Movie.destroy({ where: { id } }),
};

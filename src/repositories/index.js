const userRepository = require('./user.repository');
const movieRepository = require('./movie.repository');
const genreRepository = require('./genre.repository');
const directorRepository = require('./director.repository');
const voteRepository = require('./vote.repository');

module.exports = {
  userRepository,
  movieRepository,
  genreRepository,
  directorRepository,
  voteRepository,
};

const { Director } = require('../models');

module.exports = {
  list: (query) => Director.findAndCountAll(query),
  getById: (id) => Director.findByPk(id),
  get: (field, params) => Director.findOne({ where: { [`${field}`]: params } }),
  create: (params) => Director.create(params),
  update: (director) => director.save(),
  destroy: (id) => Director.destroy({ where: { id } }),
};

const { Vote } = require('../models');

module.exports = {
  create: (params) => Vote.create(params),
  get: (query) => Vote.findOne(query),
  destroy: (id) => Vote.destroy({ where: { id } }),
  list: (user_id) => Vote.findAll({ where: { user_id } }),
};

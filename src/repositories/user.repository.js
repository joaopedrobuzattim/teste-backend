const { User } = require('../models');

module.exports = {
  list: (query) => User.findAndCountAll(query),
  getById: (id) => User.findByPk(id),
  get: (field, params) => User.findOne({ where: { [`${field}`]: params } }),
  create: (params) => User.create(params),
  update: (user) => user.save(),
  delete: (id) => User.update({ is_active: false }, { where: { id } }),
};

const { StatusCodes } = require('http-status-codes');

class AppError {
  constructor(message, statusCode = StatusCodes.BAD_REQUEST) {
    this.message = message;
    this.statusCode = statusCode;
  }
}

module.exports = AppError;

const AppError = require('./errors/AppError');

module.exports = {
  AppError,
};

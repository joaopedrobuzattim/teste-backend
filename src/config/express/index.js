const express = require('express');
require('express-async-errors');
require('dotenv');
const routes = require('../../routes');

const { globalErrorHandler } = require('../../middlewares');
require('../../database');

const app = express();

app.set('port', process.env.PORT || 3000);

app.use(express.json());

app.use(routes);

app.use(globalErrorHandler);

module.exports = app;

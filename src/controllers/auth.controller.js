const { StatusCodes } = require('http-status-codes');
const { loginService, signupService } = require('../services/auth');

module.exports = {

  async login(request, response) {
    const { email, password } = request.body;
    const user = await loginService.exec({ email, password });

    return response.status(StatusCodes.OK).json(user);
  },

  async signup(request, response) {
    const { name, email, password } = request.body;
    const user = await signupService.exec({ name, email, password });
    return response.status(StatusCodes.OK).json(user);
  },

};

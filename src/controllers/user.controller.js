const { StatusCodes } = require('http-status-codes');
const { createUserService } = require('../services/users');
const { listUserService } = require('../services/users');
const { deleteUserService } = require('../services/users');
const { updateUserService } = require('../services/users');
const { getCurrentUserService } = require('../services/users');
const { deleteCurrentUserService } = require('../services/users');
const { updateCurrentUserService } = require('../services/users');
const { getOneUserService } = require('../services/users');

module.exports = {

  async getOne(request, response) {
    const { id } = request.params;
    const user = await getOneUserService.exec(id);

    return response.status(StatusCodes.OK).json(user);
  },

  async list(_request, response) {
    const users = await listUserService.exec();

    return response.status(StatusCodes.OK).json(users);
  },

  async create(request, response) {
    const {
      name, email, password, role,
    } = request.body;

    const user = await createUserService.exec({
      name, email, password, role,
    });

    return response.status(StatusCodes.CREATED).json(user);
  },

  async delete(request, response) {
    const { id } = request.params;
    await deleteUserService.exec(id);
    return response.status(StatusCodes.NO_CONTENT).json({ data: null });
  },

  async update(request, response) {
    const { id } = request.params;
    const data = request.body;
    const user = await updateUserService.exec(id, data);
    return response.status(StatusCodes.OK).json(user);
  },

  async getCurrentUser(request, response) {
    const { id } = request.user;
    const user = await getCurrentUserService.exec(id);

    return response.status(StatusCodes.OK).json(user);
  },

  async deleteCurrentUser(request, response) {
    const { id } = request.user;
    await deleteCurrentUserService.exec(id);
    return response.status(StatusCodes.NO_CONTENT).json({ data: null });
  },

  async updateCurrentUser(request, response) {
    const { id } = request.user;
    const data = request.body;
    const user = await updateCurrentUserService.exec(id, data);
    return response.status(StatusCodes.OK).json(user);
  },

};

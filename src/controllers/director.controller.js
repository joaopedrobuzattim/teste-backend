const { StatusCodes } = require('http-status-codes');
const {
  createDirectorService,
  updateDirectorService,
  destroyDirectorService,
  listDirectorService,
  getOneDirectorService,
} = require('../services/director');

module.exports = {

  async list(_request, response) {
    const directors = await listDirectorService.exec();

    return response.status(StatusCodes.OK).json(directors);
  },
  async getOne(request, response) {
    const { id } = request.params;

    const director = await getOneDirectorService.exec(id);
    return response.status(StatusCodes.OK).json(director);
  },
  async update(request, response) {
    const { id } = request.params;
    const { name } = request.body;

    const director = await updateDirectorService.exec(id, name);

    return response.status(StatusCodes.OK).json(director);
  },
  async destroy(request, response) {
    const { id } = request.params;

    await destroyDirectorService.exec(id);

    return response.status(StatusCodes.NO_CONTENT).json({ data: null });
  },
  async create(request, response) {
    const { name } = request.body;
    const director = await createDirectorService.exec(name);
    return response.status(StatusCodes.CREATED).json(director);
  },

};

const authController = require('./auth.controller');
const userController = require('./user.controller');
const movieController = require('./movie.controller');
const genreController = require('./genre.controller');
const directorController = require('./director.controller');
const voteController = require('./vote.controller');

module.exports = {
  authController,
  userController,
  movieController,
  genreController,
  directorController,
  voteController,
};

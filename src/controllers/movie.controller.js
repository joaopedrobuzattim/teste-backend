const { StatusCodes } = require('http-status-codes');
const { createMovieService } = require('../services/movie');
const { updateMovieService } = require('../services/movie');
const { listMovieService } = require('../services/movie');
const { getOneMovieService } = require('../services/movie');
const { deleteMovieService } = require('../services/movie');

module.exports = {

  async list(request, response) {
    const filter = request.query;
    const movies = await listMovieService.exec(filter);
    return response.status(StatusCodes.OK).json(movies);
  },
  async getOne(request, response) {
    const { id } = request.params;

    const movie = await getOneMovieService.exec(id);

    return response.status(StatusCodes.OK).json(movie);
  },

  async create(request, response) {
    const {
      name, description, genres_id, duration, director_id,
    } = request.body;

    const movie = await createMovieService.exec({
      name, description, genres_id, director_id, duration,
    });

    return response.status(StatusCodes.CREATED).json(movie);
  },

  async update(request, response) {
    const { id } = request.params;
    const data = request.body;

    const movie = await updateMovieService.exec(id, data);

    return response.status(StatusCodes.OK).json({ movie });
  },

  async destroy(request, response) {
    const { id } = request.params;

    await deleteMovieService.exec(id);

    return response.status(StatusCodes.NO_CONTENT).json({ data: null });
  },

};

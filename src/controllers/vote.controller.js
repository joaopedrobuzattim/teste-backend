const { StatusCodes } = require('http-status-codes');
const { createOrUpdateVoteService, deleteVoteService, listVoteService } = require('../services/vote');

module.exports = {

  async create(request, response) {
    const user_id = request.user.id;
    const { movie_id } = request.params;
    const { rating } = request.body;

    const vote = await createOrUpdateVoteService.exec({ user_id, movie_id, rating });

    return response.status(StatusCodes.CREATED).json(vote);
  },

  async destroy(request, response) {
    const user_id = request.user.id;
    const { movie_id } = request.params;

    await deleteVoteService.exec({ user_id, movie_id });

    return response.status(StatusCodes.NO_CONTENT).json({ data: null });
  },

  async update(request, response) {
    const user_id = request.user.id;
    const { movie_id } = request.params;
    const { rating } = request.body;

    const vote = await createOrUpdateVoteService.exec({ user_id, movie_id, rating });

    return response.status(StatusCodes.OK).json(vote);
  },

  async list(request, response) {
    const user_id = request.user.id;

    const votes = await listVoteService.exec({ user_id });

    return response.status(StatusCodes.OK).json(votes);
  },

};

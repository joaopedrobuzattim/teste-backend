const { StatusCodes } = require('http-status-codes');
const {
  createGenreService,
  updateGenreService,
  destroyGenreService,
  listGenreService,
  getOneGenreService,
} = require('../services/genre');

module.exports = {

  async list(_request, response) {
    const genres = await listGenreService.exec();

    return response.status(StatusCodes.OK).json(genres);
  },
  async getOne(request, response) {
    const { id } = request.params;

    const genre = await getOneGenreService.exec(id);
    return response.status(StatusCodes.OK).json(genre);
  },
  async update(request, response) {
    const { id } = request.params;
    const { name } = request.body;

    const genre = await updateGenreService.exec(id, name);

    return response.status(StatusCodes.OK).json(genre);
  },
  async destroy(request, response) {
    const { id } = request.params;

    await destroyGenreService.exec(id);

    return response.status(StatusCodes.NO_CONTENT).json({ data: null });
  },
  async create(request, response) {
    const { name } = request.body;
    const genre = await createGenreService.exec(name);
    return response.status(StatusCodes.CREATED).json(genre);
  },

};

const { StatusCodes } = require('http-status-codes');
const { Op } = require('sequelize');
const { AppError } = require('../../utils/index');

const { userRepository, voteRepository, movieRepository } = require('../../repositories');

module.exports.exec = async ({ user_id, movie_id }) => {
  const user = await userRepository.getById(user_id);
  if (!user) { throw new AppError('User not found!', StatusCodes.NOT_FOUND); }

  const movie = await movieRepository.getById(movie_id);
  if (!movie) { throw new AppError('Movie not found!', StatusCodes.NOT_FOUND); }

  const vote = await voteRepository.get({
    where: {
      [Op.and]: [{ user_id }, { movie_id }],
    },
  });

  if (!vote) { throw new AppError('User did not vote on this movie!', StatusCodes.NOT_FOUND); }

  await voteRepository.destroy(vote.id);

  return null;
};

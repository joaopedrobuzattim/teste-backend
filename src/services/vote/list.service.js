const { StatusCodes } = require('http-status-codes');
const { AppError } = require('../../utils/index');

const { userRepository, voteRepository } = require('../../repositories');

module.exports.exec = async ({ user_id }) => {
  const user = await userRepository.getById(user_id);
  if (!user) { throw new AppError('User not found!', StatusCodes.NOT_FOUND); }

  const votes = voteRepository.list(user_id);

  return votes;
};

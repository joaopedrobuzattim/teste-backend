const { v4: uuidv4 } = require('uuid');
const { StatusCodes } = require('http-status-codes');
const { Op } = require('sequelize');
const { AppError } = require('../../utils/index');

const { userRepository, voteRepository, movieRepository } = require('../../repositories');

module.exports.exec = async ({ user_id, movie_id, rating }) => {
  const user = await userRepository.getById(user_id);
  if (!user) { throw new AppError('User not found!', StatusCodes.NOT_FOUND); }

  const movie = await movieRepository.getById(movie_id);
  if (!movie) { throw new AppError('Movie not found!', StatusCodes.NOT_FOUND); }

  const userAlreadyVotedHere = await voteRepository.get({
    where: {
      [Op.and]: [{ user_id }, { movie_id }],
    },
  });

  if (userAlreadyVotedHere) {
    await voteRepository.destroy(userAlreadyVotedHere.id);
  }

  const id = uuidv4();

  const vote = await voteRepository.create({
    id, user_id, movie_id, rating,
  });

  return vote;
};

const createOrUpdateVoteService = require('./createOrUpdate.service');
const deleteVoteService = require('./delete.service');
const listVoteService = require('./list.service');

module.exports = {
  createOrUpdateVoteService,
  deleteVoteService,
  listVoteService,
};

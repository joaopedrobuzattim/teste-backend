const { sign } = require('jsonwebtoken');
const { StatusCodes } = require('http-status-codes');
const { encryptor } = require('../../helpers/encryptor');
const { AppError } = require('../../utils/index');
const { userRepository } = require('../../repositories');
const authConfig = require('../../config/auth/auth.config');

module.exports.exec = async ({ email, password }) => {
  const user = await userRepository.get('email', email);

  if (!user) { throw new AppError('Incorrect email/password combination.', StatusCodes.UNAUTHORIZED); }

  const passwordMatched = await encryptor.comparePassword(password, user.password);

  if (!passwordMatched) { throw new AppError('Incorrect email/password combination.', StatusCodes.UNAUTHORIZED); }

  const { secret, expiresIn } = authConfig.jwt;

  const token = sign({}, secret, {
    subject: user.id,
    expiresIn,
  });

  return { email, token };
};

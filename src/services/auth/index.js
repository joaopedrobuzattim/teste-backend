const loginService = require('./login.service');
const signupService = require('./signup.service');

module.exports = {
  loginService,
  signupService,
};

const { v4: uuidv4 } = require('uuid');
const { sign } = require('jsonwebtoken');
const { StatusCodes } = require('http-status-codes');
const { AppError } = require('../../utils/index');
const authConfig = require('../../config/auth/auth.config');
const { userRepository } = require('../../repositories');

module.exports.exec = async ({ name, email, password }) => {
  const checkIfUserExists = await userRepository.get('email', email);

  if (checkIfUserExists) { throw new AppError(`User ${email} already exist!!`, StatusCodes.CONFLICT); }

  const userId = uuidv4();

  const user = await userRepository.create({
    id: userId,
    name,
    email,
    password,
  });

  const { secret, expiresIn } = authConfig.jwt;

  const token = sign({}, secret, {
    subject: user.id,
    expiresIn,
  });

  return { user, token };
};

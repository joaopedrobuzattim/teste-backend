const { StatusCodes } = require('http-status-codes');
const { v4: uuidv4 } = require('uuid');
const { AppError } = require('../../utils/index');
const { directorRepository } = require('../../repositories');

module.exports.exec = async (name) => {
  const directorAlreadyExists = await directorRepository.get('name', name);

  if (directorAlreadyExists) { throw new AppError(`Director ${name} alreay exists!`, StatusCodes.CONFLICT); }

  const directorId = uuidv4();

  const director = await directorRepository.create({
    id: directorId,
    name,
  });

  return director;
};

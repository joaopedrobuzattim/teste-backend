const createDirectorService = require('./create.service');
const updateDirectorService = require('./update.service');
const destroyDirectorService = require('./destroy.service');
const listDirectorService = require('./list.service');
const getOneDirectorService = require('./getOne.service');

module.exports = {
  createDirectorService,
  updateDirectorService,
  destroyDirectorService,
  listDirectorService,
  getOneDirectorService,
};

const { directorRepository } = require('../../repositories');

module.exports.exec = async () => {
  const directors = await directorRepository.list({});

  return directors;
};

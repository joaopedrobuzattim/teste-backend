const { StatusCodes } = require('http-status-codes');
const { AppError } = require('../../utils/index');
const { directorRepository } = require('../../repositories');

module.exports.exec = async (id, name) => {
  const director = await directorRepository.getById(id);

  if (!director) { throw new AppError('Director does not exist!', StatusCodes.NOT_FOUND); }

  const directorAlreadyExists = await directorRepository.get('name', name);

  if (directorAlreadyExists) { throw new AppError(`Director ${name} alreay exists!`, StatusCodes.CONFLICT); }

  director.name = name;

  const directorUpdated = await directorRepository.update(director);

  return directorUpdated;
};

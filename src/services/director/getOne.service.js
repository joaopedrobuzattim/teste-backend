const { StatusCodes } = require('http-status-codes');
const { AppError } = require('../../utils/index');
const { directorRepository } = require('../../repositories');

module.exports.exec = async (id) => {
  const director = await directorRepository.getById(id);

  if (!director) { throw new AppError('Director not found!', StatusCodes.NOT_FOUND); }

  return director;
};

const { StatusCodes } = require('http-status-codes');
const { AppError } = require('../../utils/index');
const { userRepository } = require('../../repositories');

module.exports.exec = async (id) => {
  const checkIfUserExists = await userRepository.getById(id);

  if (!checkIfUserExists.is_active) { throw new AppError('User not found!', StatusCodes.NOT_FOUND); }

  const user = await userRepository.delete(id);

  return user;
};

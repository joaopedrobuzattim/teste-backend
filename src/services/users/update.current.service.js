const { StatusCodes } = require('http-status-codes');
const { userRepository } = require('../../repositories');
const { AppError } = require('../../utils/index');

module.exports.exec = async (id, data) => {
  const user = await userRepository.getById(id);

  if (!user || !user.is_active) { throw new AppError('User not found!!', StatusCodes.NOT_FOUND); }

  const { email } = data;

  if (email) {
    const checkEmail = await userRepository.get('email', email);
    if (checkEmail) { throw new AppError(`User ${email} already exists!`, StatusCodes.CONFLICT); }
  }

  const fields = Object.keys(data);

  fields.forEach((current_value) => {
    user[current_value] = data[current_value];
  });

  const updatedUser = await userRepository.update(user);

  return updatedUser;
};

const getOneUserService = require('./getOne.service');
const createUserService = require('./create.service');
const deleteUserService = require('./delete.service');
const listUserService = require('./list.service');
const updateUserService = require('./update.service');
const getCurrentUserService = require('./get.current.service');
const deleteCurrentUserService = require('./delete.current.service');
const updateCurrentUserService = require('./update.current.service');

module.exports = {
  createUserService,
  deleteUserService,
  listUserService,
  updateUserService,
  getCurrentUserService,
  deleteCurrentUserService,
  updateCurrentUserService,
  getOneUserService,
};

const { userRepository } = require('../../repositories');

module.exports.exec = async () => {
  const query = {
    where: {
      is_active: true,
    },
  };

  const users = await userRepository.list(query);

  return users;
};

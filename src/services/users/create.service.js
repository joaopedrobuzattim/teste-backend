const { v4: uuidv4 } = require('uuid');
const { StatusCodes } = require('http-status-codes');
const { AppError } = require('../../utils/index');
const { userRepository } = require('../../repositories');

module.exports.exec = async ({
  name, email, password, role,
}) => {
  const checkUserExists = await userRepository.get('email', email);

  if (checkUserExists) { throw new AppError(`User ${email} already exists!`, StatusCodes.CONFLICT); }

  const userId = uuidv4();

  const user = userRepository.create({
    id: userId,
    name,
    email,
    password,
    role,
  });

  return user;
};

const { StatusCodes } = require('http-status-codes');
const { AppError } = require('../../utils/index');
const { userRepository } = require('../../repositories');

module.exports.exec = async (id) => {
  const user = await userRepository.getById(id);

  if (!user) { throw new AppError('User not found!', StatusCodes.NOT_FOUND); }

  return user;
};

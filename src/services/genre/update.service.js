const { StatusCodes } = require('http-status-codes');
const { AppError } = require('../../utils/index');
const { genreRepository } = require('../../repositories');

module.exports.exec = async (id, name) => {
  const genre = await genreRepository.getById(id);

  if (!genre) { throw new AppError('Genre does not exist!', StatusCodes.NOT_FOUND); }

  const genreAlreadyExists = await genreRepository.get('name', name);

  if (genreAlreadyExists) { throw new AppError(`Genre ${name} alreay exists!`, StatusCodes.CONFLICT); }

  genre.name = name;

  const genreUpdated = await genreRepository.update(genre);

  return genreUpdated;
};

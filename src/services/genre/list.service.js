const { genreRepository } = require('../../repositories');

module.exports.exec = async () => {
  const genres = await genreRepository.list({});

  return genres;
};

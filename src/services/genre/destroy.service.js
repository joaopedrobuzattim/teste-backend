const { StatusCodes } = require('http-status-codes');
const { AppError } = require('../../utils/index');
const { genreRepository } = require('../../repositories');

module.exports.exec = async (id) => {
  const genre = await genreRepository.getById(id);

  if (!genre) { throw new AppError('Genre does not exist!', StatusCodes.NOT_FOUND); }

  await genreRepository.destroy(id);

  return null;
};

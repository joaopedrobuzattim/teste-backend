const createGenreService = require('./create.service');
const updateGenreService = require('./update.service');
const destroyGenreService = require('./destroy.service');
const listGenreService = require('./list.service');
const getOneGenreService = require('./getOne.service');

module.exports = {
  createGenreService,
  updateGenreService,
  destroyGenreService,
  listGenreService,
  getOneGenreService,
};

const { StatusCodes } = require('http-status-codes');
const { v4: uuidv4 } = require('uuid');
const { AppError } = require('../../utils/index');
const { genreRepository } = require('../../repositories');

module.exports.exec = async (name) => {
  const genreAlreadyExists = await genreRepository.get('name', name);

  if (genreAlreadyExists) { throw new AppError(`Genre ${name} alreay exists!`, StatusCodes.CONFLICT); }

  const genreId = uuidv4();

  const genre = await genreRepository.create({
    id: genreId,
    name,
  });

  return genre;
};

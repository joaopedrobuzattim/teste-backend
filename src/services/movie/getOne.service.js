const { StatusCodes } = require('http-status-codes');
const { AppError } = require('../../utils/index');
const { Genre, Director, Vote } = require('../../models');
const { movieRepository } = require('../../repositories');

module.exports.exec = async (id) => {
  const movie = await movieRepository.getOne({
    where: {
      id,
    },
    include: [
      {
        model: Director,
        attributes: ['name'],
      },
      {
        model: Genre,
        attributes: ['name'],
        through: {
          attributes: [],
        },
      },
      {
        model: Vote,
        attributes: ['rating'],
      },
    ],
  });

  if (movie.length === 0) { throw new AppError('Movie not found!!', StatusCodes.NOT_FOUND); }

  return movie;
};

const createMovieService = require('./create.service');
const updateMovieService = require('./update.service');
const listMovieService = require('./list.service');
const getOneMovieService = require('./getOne.service');
const deleteMovieService = require('./delete.service');

module.exports = {
  createMovieService,
  updateMovieService,
  listMovieService,
  getOneMovieService,
  deleteMovieService,
};

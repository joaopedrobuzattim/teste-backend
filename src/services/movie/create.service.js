const { v4: uuidv4 } = require('uuid');
const { StatusCodes } = require('http-status-codes');
const { AppError } = require('../../utils/index');
const { movieRepository } = require('../../repositories');
const { directorRepository } = require('../../repositories');
const { genreRepository } = require('../../repositories');

module.exports.exec = async ({
  name, description, director_id, duration, genres_id,
}) => {
  genres_id.forEach(async (current_genre_id) => {
    const genre = await genreRepository.getById(current_genre_id);

    if (!genre) {
      throw new AppError('Genre does not exist!', StatusCodes.NOT_FOUND);
    }
  });

  const director = await directorRepository.getById(director_id);

  if (!director) {
    throw new AppError('Director does not exist!', StatusCodes.NOT_FOUND);
  }

  const movie_id = uuidv4();

  const movie = await movieRepository.create({
    id: movie_id,
    name,
    description,
    duration,
    director_id,
  });

  movie.setGenres(genres_id);

  return {
    ...movie.dataValues,
    genres_id,
  };
};

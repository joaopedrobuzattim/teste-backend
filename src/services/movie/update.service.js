const { StatusCodes } = require('http-status-codes');
const { movieRepository } = require('../../repositories');
const { directorRepository } = require('../../repositories');
const { genreRepository } = require('../../repositories');
const { AppError } = require('../../utils/index');

module.exports.exec = async (id, { genres_id, director_id, ...data }) => {
  const movie = await movieRepository.getById(id);

  if (!movie) { throw new AppError('Movie not found!!', StatusCodes.NOT_FOUND); }

  const fields = Object.keys(data);

  if (genres_id) {
    genres_id.forEach(async (current_genre_id) => {
      const genre = await genreRepository.getById(current_genre_id);

      if (!genre) {
        throw new AppError('Genre does not exist!', StatusCodes.NOT_FOUND);
      }
    });
    await movie.setGenres(genres_id);
  }

  if (director_id) {
    const director = await directorRepository.getById(director_id);

    if (!director) {
      throw new AppError('Director does not exist!', StatusCodes.NOT_FOUND);
    }
    movie.director_id = director_id;
  }

  fields.forEach(async (current_value) => {
    movie[current_value] = data[current_value];
  });

  const genres = await movie.getGenres({
    attributes: ['id'],
  });

  const updatedMovie = await movieRepository.update(movie);
  return {
    ...updatedMovie.dataValues,
    genres,
  };
};

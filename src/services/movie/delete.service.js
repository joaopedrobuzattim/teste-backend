const { StatusCodes } = require('http-status-codes');
const { AppError } = require('../../utils/index');
const { movieRepository } = require('../../repositories');

module.exports.exec = async (id) => {
  const movie = await movieRepository.getById(id);

  if (!movie) { throw new AppError('Movie not found!!', StatusCodes.NOT_FOUND); }

  await movieRepository.destroy(id);

  return null;
};

const sequelize = require('sequelize');
const { Op } = require('sequelize');
const { movieRepository } = require('../../repositories');
const { Genre, Director, Vote } = require('../../models');

module.exports.exec = async (param) => {
  if (param.name) {
    const query = {
      where: {
        name: {
          [Op.startsWith]: param.name,
        },
      },
      include: [
        {
          model: Director,
          attributes: ['name'],
        },
        {
          model: Genre,
          attributes: ['name'],
          through: {
            attributes: [],
          },
        },
        {
          model: Vote,
          attributes: ['rating'],
        },
      ],
    };
    const movies = await movieRepository.list(query);
    return { movies };
  }
  if (param.director) {
    const query = {
      include: [
        {
          model: Director,
          attributes: ['name'],
          where: {
            name: { [Op.startsWith]: param.director },
          },
        },
        {
          model: Genre,
          attributes: ['name'],
          through: {
            attributes: [],
          },
        },
        {
          model: Vote,
          attributes: ['rating'],
        },
      ],
    };
    const movies = await movieRepository.list(query);
    return movies;
  }
  if (param.genre) {
    const query = {
      include: [
        {
          model: Director,
          attributes: ['name'],
        },
        {
          model: Genre,
          attributes: ['name'],
          where: {
            name: { [Op.startsWith]: param.genre },
          },
          through: {
            attributes: [],
          },
        },
        {
          model: Vote,
          attributes: ['rating'],
        },
      ],
    };
    const movies = await movieRepository.list(query);
    return movies;
  }

  const movies = await movieRepository.list({
    include: [
      {
        model: Director,
        attributes: ['name'],
      },
      {
        model: Genre,
        attributes: ['name'],
        through: {
          attributes: [],
        },
      },
      {
        model: Vote,
        attributes: ['rating'],
      },
    ],
  });
  return movies;
};

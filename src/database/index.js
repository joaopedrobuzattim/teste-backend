const Sequelize = require('sequelize');
const dbConfig = require('../config/database/database.config');

const connection = new Sequelize(dbConfig);

module.exports = connection;

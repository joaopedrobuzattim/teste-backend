module.exports = {
  up: async (queryInterface, _Sequelize) => queryInterface.bulkInsert('users', [
    {
      id: '4ec89df3-75d0-49de-adcd-b02bc22c7213',
      name: 'IMDB Admin',
      email: 'admin@imdb.com',
      password: '$2y$10$27TKGXRJWP4F.W53VId.aedxVWWr7guPNTw6weQ5gGQ2WnbA4WkJy',
      role: 'admin',
      is_active: true,
      created_at: new Date(),
      updated_at: new Date(),
    },
  ]),

  down: async (queryInterface, _Sequelize) => queryInterface.bulkDelete('users', null, {}),
};

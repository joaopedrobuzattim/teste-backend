const { Router } = require('express');
const { ensureAuthenticated } = require('../middlewares');
const { restrictTo } = require('../middlewares');
const { directorController } = require('../controllers');
const { directorValidate } = require('../validations');

const router = Router();

router.use(ensureAuthenticated);
router.use(restrictTo('admin'));

router.get('/', directorController.list);
router.get('/:id', directorController.getOne);
router.post('/', directorValidate.createDirectorValidate, directorController.create);
router.put('/:id', directorValidate.updateDirectorValidate, directorController.update);
router.delete('/:id', directorController.destroy);

module.exports = router;

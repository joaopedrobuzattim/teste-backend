const { Router } = require('express');

const userRoutes = require('./user.routes');
const authRoutes = require('./auth.routes');
const movieRoutes = require('./movie.routes');
const genreRoutes = require('./genre.routes');
const directorRoutes = require('./director.routes');

const routes = Router();

routes.use('/api/v1/users', userRoutes);
routes.use('/api/v1/auth', authRoutes);
routes.use('/api/v1/movies', movieRoutes);
routes.use('/api/v1/genres', genreRoutes);
routes.use('/api/v1/directors', directorRoutes);

module.exports = routes;

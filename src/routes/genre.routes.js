const { Router } = require('express');
const { ensureAuthenticated } = require('../middlewares');
const { restrictTo } = require('../middlewares');
const { genreController } = require('../controllers');
const { genreValidate } = require('../validations');

const router = Router();

router.use(ensureAuthenticated);
router.use(restrictTo('admin'));

router.get('/', genreController.list);
router.get('/:id', genreController.getOne);
router.post('/', genreValidate.createGenreValidate, genreController.create);
router.put('/:id', genreValidate.updateGenreValidate, genreController.update);
router.delete('/:id', genreController.destroy);

module.exports = router;

const { Router } = require('express');
const { ensureAuthenticated } = require('../middlewares');
const { restrictTo } = require('../middlewares');

const router = Router();

router.use(ensureAuthenticated);
router.use(restrictTo('user'));

router.post('/:movieId');
router.delete('/:moveId');
router.put('/:moveId');

module.exports = router;

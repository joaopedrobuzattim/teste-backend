const { Router } = require('express');
const { userController, voteController } = require('../controllers');
const { ensureAuthenticated } = require('../middlewares');
const { restrictTo } = require('../middlewares');
const { userValidate } = require('../validations');

const router = Router();

router.use(ensureAuthenticated);

router.get('/me', userController.getCurrentUser);
router.get('/me/votes', restrictTo('user'), voteController.list);
router.delete('/me', userController.deleteCurrentUser);
router.patch('/me', userValidate.updateCurrentValidate, userController.updateCurrentUser);

router.use(restrictTo('admin'));

router.post('/', userValidate.createUserValidate, userController.create);
router.get('/', userController.list);
router.get('/:id', userController.getOne);
router.delete('/:id', userController.delete);
router.patch('/:id', userValidate.updateUserValidate, userController.update);

module.exports = router;

const { Router } = require('express');
const { ensureAuthenticated } = require('../middlewares');
const { movieController, voteController } = require('../controllers');
const { movieValidate, voteValidate } = require('../validations');
const { restrictTo } = require('../middlewares');

const router = Router();

router.get('/', movieController.list);
router.get('/:id', movieController.getOne);

router.use(ensureAuthenticated);

router.post('/:movie_id/votes', voteValidate.createVoteValidate, restrictTo('user'), voteController.create);
router.delete('/:movie_id/votes', restrictTo('user'), voteController.destroy);
router.patch('/:movie_id/votes', voteValidate.updateVoteValidate, restrictTo('user'), voteController.update);

router.use(restrictTo('admin'));

router.post('/', movieValidate.createMovieValidate, movieController.create);
router.patch('/:id', movieValidate.updateMovieValidate, movieController.update);
router.delete('/:id', movieController.destroy);

module.exports = router;

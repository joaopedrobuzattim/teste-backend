const { Router } = require('express');
const { authController } = require('../controllers');
const { authValidate } = require('../validations');

const router = Router();

router.post('/login', authValidate.loginValidate, authController.login);
router.post('/signup', authValidate.signupValidate, authController.signup);

module.exports = router;
